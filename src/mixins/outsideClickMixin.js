export const outsideClick = {
  inheritAttrs: false,
  data(){
    return{
      isFocused: false,
    }
  },
  created(){
    document.addEventListener('click', (e) => {
      if(e.path.filter((i) => { return i === this.$el}).length === 0){
        return this.isFocused = false
      }
    })
  }
};
