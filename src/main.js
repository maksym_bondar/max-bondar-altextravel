import Vue from 'vue'
import store from './store'
import App from './App.vue'
import router from './router/index'

// PLUGINS
import AirbnbStyleDatepicker from 'vue-airbnb-style-datepicker'
import Vuelidate from 'vuelidate'
// PLUGINS

// STYLES
import 'vue-airbnb-style-datepicker/dist/vue-airbnb-style-datepicker.min.css'
import 'epic-spinners/dist/lib/epic-spinners.min.css'
// STYLES

import DatepickerOptions from '../src/js/datepickerConfig'

Vue.use(AirbnbStyleDatepicker, DatepickerOptions);
Vue.use(Vuelidate);

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
});

