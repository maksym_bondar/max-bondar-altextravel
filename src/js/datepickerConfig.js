export default {
  colors: {
    selected: '#C5E1F9',
    selectedText: '#363637',
    inRange: '#E8F3FD',
    inRangeBorder: 'none',
    text: '#363637',
  },
  daysShort: ['M', 'T', 'W', 'T ', 'F', 'S', 'S '],
  disableOutsideRangeNavigation: true
}

