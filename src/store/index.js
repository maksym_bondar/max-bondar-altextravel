import Vue from 'vue'
import 'es6-promise/auto'
import Vuex from 'vuex';
import roundTrip from './roundTrip'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    roundTrip,
  }
});
