import api from '../services/apiService'

export default {
  state: {
    passengersCount: {
      adultsCount: 1,
      childrenCount: 0,
      infantsCount: 0,
    },
    cabinType: 'Economy',
    origin: {},
    destination: {},
    departureDate: '',
    returnDate: '',
    originLocations: [],
    destinationLocations: [],
    roundTripSearchResult: [],
    isPending: false,
  },
  getters: {
    originLocations: state => {
      let arr = [],
          index = 0;

      state.originLocations.forEach(location => {
        arr.push({
          id: index,
          code: location.code,
          title: location.name,
        });
        index++;
        if(location.airports !== undefined) {
          location.airports.forEach(airport => {
              arr.push({
                id: index,
                code: airport.code,
                title: airport.name,
              });
              index++
            })
          }
        });
  console.log(arr);
    return arr
    },
    destinationLocations: state => {
      let arr = [],
        index = 0;

      state.destinationLocations.forEach(location => {
        arr.push({
          id: index,
          code: location.code,
          title: location.name,
        });
        index++;
        if(location.airports !== undefined) {
          location.airports.forEach(airport => {
            arr.push({
              id: index,
              code: airport.code,
              title: airport.name,
            });
            index++
          })
        }
      });
      console.log(arr);
      return arr
    },
  },
  mutations: {
    CHANGE_PASSENGERS(state, payload){
      state.passengersCount = payload;
    },
    SELECTED_CABIN_TYPE(state, payload){
      state.selectedCabinType = payload;
    },
    SELECTED_ORIGIN_POINT(state, payload){
      state.origin = payload;
    },
    SELECTED_DESTINATION_POINT(state, payload){
      state.destination = payload;
    },
    SELECTED_DEPARTURE_DATE(state, payload){
      state.departureDate = payload;
    },
    SELECTED_RETURN_DATE(state, payload){
      state.returnDate = payload;
    },
    SWITCH_ORIGIN_DESTINATION_POINT(state){
      const temp = state.origin;
      state.origin = state.destination;
      state.destination = temp;
    },
    SET_ORIGIN_LOCATIONS(state, payload){
      state.originLocations = payload;
    },
    SET_DESTINATION_LOCATIONS(state, payload){
      state.destinationLocations = payload;
    },
    SET_ROUND_TRIP_SEARCH_RESULT(state, payload){
      state.roundTripSearchResult = payload;
    },
    CHANGE_PENDING_STATUS(state, payload){
      state.isPending = payload;
    },
  },
  actions: {
    changePassengers({commit}, payload){
      commit('CHANGE_PASSENGERS', payload)
    },
    setSelectedCabinType({ commit }, payload){
      commit('SELECTED_CABIN_TYPE', payload)
    },
    setSelectedOriginPoint({ commit }, payload){
      commit('SELECTED_ORIGIN_POINT', payload)
    },
    setSelectedDestinationPoint({ commit }, payload){
      commit('SELECTED_DESTINATION_POINT', payload)
    },
    setDepartureDate({ commit }, payload){
      commit('SELECTED_DEPARTURE_DATE', payload)
    },
    setReturnDate({ commit }, payload){
      commit('SELECTED_RETURN_DATE', payload)
    },
    switchOriginDestinationPoints({ commit }){
      commit('SWITCH_ORIGIN_DESTINATION_POINT');
    },
    getOriginLocations({commit},payload){
      return api.getLocations(payload)
        .then(response => {
          commit('SET_ORIGIN_LOCATIONS', response.data)
        })
        .catch(error => console.error(error))
        .finally(() => { console.log('finally'); })
    },
    getDestinationLocations({commit},payload){
      return api.getLocations(payload)
        .then(response => {
          commit('SET_DESTINATION_LOCATIONS', response.data)
        })
        .catch(error => console.error(error))
        .finally(() => { console.log('finally'); })
    },
    getRoundTripResult({commit},payload){
      commit('CHANGE_PENDING_STATUS', true);
      return api.getRoundTrip(payload)
        .then(response => {
          console.log(response);
          commit('SET_ROUND_TRIP_SEARCH_RESULT', response.data);
        })
        .catch(error => console.error(error))
        .finally(() => {
          commit('CHANGE_PENDING_STATUS', false);
        })
    }
  }
}

