import axios from 'axios'

export default {
  getLocations(search){
    return axios.get(`https://localhost:44346/api/Locations?search=${search}&count=10`)
  },
  getRoundTrip(data){
    return axios.get(`https://localhost:44346/api/flights/search/roundtrip?ReturnDate=${data.returnDate}&DepartureDate=${data.departureDate}&Route.ArrivalPort=${data.destination}&Route.DeparturePort=${data.origin}&Cabin=${data.cabinType}&CurrencyCode=USD&DemandDirectFlight=false&NumberOfAdults=${data.adultsNumber}&NumberOfChildren=${data.childrenNumber}&NumberOfInfants=${data.infantsNumber}`)
  }
}
