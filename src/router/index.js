import Vue from 'vue'
import VueRouter from 'vue-router'
import SearchFlights from '../components/SearchFlights'
import SearchFlightsResult from '../components/SearchFlightsResult'

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'SearchFlights',
      component: SearchFlights,
    },
    {
      path: '/search-flights-result',
      name: 'SearchFlightsResult',
      component: SearchFlightsResult,
    }
  ]
})
